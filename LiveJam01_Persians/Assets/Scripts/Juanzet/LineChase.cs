using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineChase : MonoBehaviour
{
    [SerializeField]
    LineRenderer line;

    public Transform thisPlayer;
    public Transform otherPlayer;

    void Update()
    {
        LineLogic();
    }

    void LineLogic()
    {
        line.SetPosition(0, thisPlayer.position);
        line.SetPosition(1, otherPlayer.position);
    }
}
