using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attraction : MonoBehaviour
{
    [SerializeField] string tagToAttract = "";
    GameObject ObjToAttract; //determina q objeto va a atraer en base al tag 
    public float force;
    bool inArea;

    public void AttractObject(Rigidbody2D obj)
    {
        Vector2 dir = (transform.position - obj.gameObject.transform.position) * force;

        obj.AddForce(dir);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(tagToAttract))
        {
            inArea = true;
            ObjToAttract = collision.gameObject;

            // activar shader 
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag(tagToAttract)) { inArea = false; }           
    }

    private void FixedUpdate()
    {
        if (inArea)
        {
            AttractObject(ObjToAttract.GetComponent<Rigidbody2D>());
        }
    }
}