using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBehavior : MonoBehaviour
{
    public enum ObjectType { sad, happy}

    public ObjectType objectType;

    public float influenceForce; // con que valor va a afectar al jugador
   public void PickUpObject(GameObject player)
   {
        Vector3 newScale = new Vector3(transform.localScale.x / 4, transform.localScale.y / 4, transform.localScale.z / 4);

        transform.localScale = newScale;
        transform.SetParent(player.transform);
        if (transform.childCount > 0)
        {
            Destroy(transform.GetChild(0).gameObject);
        }
    }
}
