using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInteractions : MonoBehaviour
{
    CharacterBehaviour characterBehavior;

    private void Start()
    {
        characterBehavior = GetComponentInParent<CharacterBehaviour>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("InteractableObj"))
        {
            Debug.Log("TocasteObj");

            ObjectBehavior obj = collision.GetComponent<ObjectBehavior>();

            if (obj.objectType == ObjectBehavior.ObjectType.sad)
            {
                characterBehavior.IncreaseSadness(obj.influenceForce);
            }
            if (obj.objectType == ObjectBehavior.ObjectType.happy)
            {
                characterBehavior.ReduceSadness(obj.influenceForce);
            }
            collision.GetComponent<ObjectBehavior>().PickUpObject(this.gameObject);                
        }


        // ABRAZO 

        //if (collision.gameObject.CompareTag("Player"))
        //{
        //    collision.gameObject.GetComponent<CharacterBehaviour>().Sadness = collision.gameObject.GetComponent<CharacterBehaviour>().ReduceSadness();
        //    this.gameObject.GetComponent<CharacterBehaviour>().Sadness = this.gameObject.GetComponent<CharacterBehaviour>().ReduceSadness();
        //}
    }
}
