using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emotions : MonoBehaviour
{
    public enum EmotionalState { Happy, Normal, Uncomfortable, Sad }

    public EmotionalState CharacterEmotion;


    public EmotionalState GetCurrentEmotionalState()
    {
        return this.CharacterEmotion;
    }

    public EmotionalState SetEmotionalState(EmotionalState pEmotion)
    {
        this.CharacterEmotion = pEmotion;

        return this.CharacterEmotion;
    }

}
