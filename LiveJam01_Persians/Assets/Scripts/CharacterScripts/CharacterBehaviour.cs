using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBehaviour : MonoBehaviour
{
    public int Sadness;

    float topSadness = 100f;

    public float TimeIdle, cdTime;

    float iTime = 0f;

    public bool isActive, moving;

    Emotions emotion;

    public Sprite[] characterEmotions;

    Rigidbody2D _rigidbody;




    private void Awake()
    {
        emotion = this.GetComponent<Emotions>();
        _rigidbody = this.GetComponent<Rigidbody2D>();
    }


    private void ChangePlayerSprite()
    {
        switch (emotion.GetCurrentEmotionalState())
        {
            case Emotions.EmotionalState.Happy:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = characterEmotions[0];
                break;
            case Emotions.EmotionalState.Normal:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = characterEmotions[1];
                break;
            case Emotions.EmotionalState.Uncomfortable:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = characterEmotions[2];
                break;
            case Emotions.EmotionalState.Sad:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = characterEmotions[3];
                break;
        }
    }

    private void ChangeEmotion()
    {
        if (Sadness <= 0)
            emotion.SetEmotionalState(Emotions.EmotionalState.Happy);
        else if(Sadness > 0 && Sadness < 50)
            emotion.SetEmotionalState(Emotions.EmotionalState.Normal);
        else if(Sadness > 50 && Sadness < 80)
            emotion.SetEmotionalState(Emotions.EmotionalState.Uncomfortable);
        else
            emotion.SetEmotionalState(Emotions.EmotionalState.Sad);
    }

    private void EnablePlayerMovement()
    {
        if (isActive)
            this.GetComponent<CharacterMovement>().enabled = true;
        else
            this.GetComponent<CharacterMovement>().enabled = false;
    }

    private void CharacterMassChange()
    { 

        // Zurdo cambio valores de las masas
        if (Sadness <= 0)
            _rigidbody.mass = 1;
        else if (Sadness > 0 && Sadness < 50)
            _rigidbody.mass = 1.5f;
        else if (Sadness > 50 && Sadness < 80)
            _rigidbody.mass = 2.5f;
        else if (Sadness > 80 && Sadness < 100)
            _rigidbody.mass = 4;
        //else
            //lose;
    }

    public void ReduceSadness(float value)
    {
        if (this.Sadness >= 50) // si esta muy triste suma menos 
            Sadness -= (int)(value * topSadness); // 0.02
        else if(this.Sadness >= 0 && this.Sadness < 50) // si esta un poco mejor suma mas 
            Sadness -= (int)(value * topSadness); // 0.05

        if (this.Sadness < 0)
            this.Sadness = 0;

    }
    public void IncreaseSadness(float value)
    {
        if (this.Sadness >= 50)
            Sadness += (int)(value * topSadness); // 0.02
        else if (this.Sadness >= 0 && this.Sadness < 50)
            Sadness += (int)(value * 2 *  topSadness); // 0.05
    }

    private void Start()
    {
        ChangePlayerSprite();      
    }

    private void Update()
    {
        ChangeEmotion();
        ChangePlayerSprite();
        CharacterMassChange();

        //EnablePlayerMovement();

        if (!moving && Sadness < 50)
        {
            if (iTime == 0)
            {
                StartCoroutine(AddSadnessPointsRoutine(TimeIdle));
            }
        }
        else if (Sadness >= 50)
        {
            if (iTime == 0)
            {
                StartCoroutine(AddSadnessPointsRoutine(TimeIdle));
            }
        }
        else
        {
            StopAllCoroutines();
            iTime = 0;
        }
    }

    IEnumerator AddSadnessPointsRoutine(float pTime)
    {
        iTime = pTime;

        while(iTime > 0)
        {
            yield return new WaitForSecondsRealtime(1.0f);
            iTime--;
        }

        IncreaseSadness(.03f);

    }
}
