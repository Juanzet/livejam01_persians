using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float speed;

    Rigidbody2D _rigidbody;

    

    private void Awake()
    {
        _rigidbody = this.GetComponent<Rigidbody2D>();
    }


    private void MoveCharacter()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");


        _rigidbody.AddForce(new Vector2(x * speed, y * speed));


    }

    private void flipCharacter()
    {
        float xValue = Input.GetAxisRaw("Horizontal");
       
        if (xValue > 0)
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        else if(xValue < 0)
            this.transform.rotation = Quaternion.Euler(0, 180, 0);

    }

    private void Update()
    {
        flipCharacter();
    }

    private void FixedUpdate()
    {
        MoveCharacter();
    }


}
