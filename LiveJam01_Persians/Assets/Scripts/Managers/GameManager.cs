using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public CharacterBehaviour Sadness;

    public LineRenderer Thread;

    public Image SadnessBar, Fade;

    

    private void Start()
    {
        SadnessBar.fillAmount = (float)Sadness.Sadness / 100;
    }

    public void Win()
    {
        StartCoroutine(FadeRoutine(3f, "Final"));
    }

    private void Retry()
    {
        Thread.gameObject.SetActive(true);
        StartCoroutine(FadeRoutine(3f, "Retry"));
    }

    private void Update()
    {
        SadnessBar.fillAmount = (float)Sadness.Sadness / 100;

        if(Sadness.Sadness >= 100)
        {
            Retry();
        }
    }


    IEnumerator FadeRoutine(float pTime, string Scene)
    {
        float mTime = pTime;

        Fade.gameObject.SetActive(true);

        while(mTime > 0)
        {
            yield return new WaitForSecondsRealtime(1.0f);
            mTime--;
        }

        SceneManager.LoadScene(Scene);
    }

}
