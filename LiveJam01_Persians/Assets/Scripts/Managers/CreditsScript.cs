using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsScript : MonoBehaviour
{
    public GameObject Credits;

    public float Speed;


    private void Start()
    {
       
    }

    private void Update()
    {
        if (Credits.transform.position.y < 5200)
            Credits.transform.position += new Vector3(0, 1 * Speed * Time.deltaTime);
        else
            SceneManager.LoadScene("MainMenu");
    }


}
