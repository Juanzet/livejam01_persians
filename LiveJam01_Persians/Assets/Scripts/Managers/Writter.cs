using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class Writter : MonoBehaviour
{
    [SerializeField] private UnityEvent textFinished;

    TMP_Text text_UI;
    string textToWrite;
    int letterIndex;
    float timePerLetter;
    float timer;


    public void AddWritter(TMP_Text text, string ttw, float tpl)
    {
        this.text_UI = text;
        this.textToWrite = ttw;
        this.timePerLetter = tpl;
        letterIndex = 0;
    }


    private void Update()
    {
        if(text_UI != null)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                timer += timePerLetter;
                letterIndex++;
                text_UI.text = textToWrite.Substring(0, letterIndex);


                if(letterIndex >= textToWrite.Length)
                {
                    textFinished.Invoke();
                    text_UI = null;
                    return;
                }
            }
        }
    }

}
