using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndingWScripts : MonoBehaviour
{
    public Writter writter;

    public TMP_Text[] texts;

    public TMP_Text txtT;

    public float timeOfWrite;

    public Image Final;


    private void Start()
    {
        StartCoroutine(ChangeScene(8f));
    }
    IEnumerator ChangeScene(float pTime)
    {

        writter.AddWritter(txtT, texts[0].text, timeOfWrite);

        yield return new WaitForSecondsRealtime(pTime);

        writter.AddWritter(txtT, texts[1].text, timeOfWrite);

        yield return new WaitForSecondsRealtime(pTime);

        writter.AddWritter(txtT, texts[2].text, timeOfWrite);

        yield return new WaitForSecondsRealtime(pTime);

        writter.AddWritter(txtT, texts[3].text, timeOfWrite);

        yield return new WaitForSecondsRealtime(pTime);

        writter.AddWritter(txtT, texts[4].text, timeOfWrite);

        yield return new WaitForSecondsRealtime(pTime);

        writter.AddWritter(txtT, texts[5].text, timeOfWrite);

        yield return new WaitForSecondsRealtime(pTime);

        writter.AddWritter(txtT, texts[6].text, timeOfWrite);

        yield return new WaitForSecondsRealtime(pTime);

        writter.AddWritter(txtT, texts[7].text, timeOfWrite);

        yield return new WaitForSecondsRealtime(pTime);

        Final.gameObject.SetActive(true);

        yield return new WaitForSecondsRealtime(pTime);

        SceneManager.LoadScene("Credits");


    }

}
